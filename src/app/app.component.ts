import { faUser, faCaretDown, faGlobeAsia, faPencilAlt } from '@fortawesome/free-solid-svg-icons';
import { AccountModalComponent } from './account-modal/account-modal.component';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AccountService } from './account.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'quantifeed';
  bsModalRef: BsModalRef;
  public client: any;
  public faUser = faUser;
  public faCaretDown = faCaretDown;
  public faGlobeAsia = faGlobeAsia;
  public faPencilAlt = faPencilAlt;
  public subsciption: any;
  constructor(
    private modalService: BsModalService,
    private accService: AccountService
  ) {
    this.getClient('001');
  }

  async getClient(client) {
    try {
      var res: any = await this.accService.getReq(`getClient?client=${client}`);
      this.client = res.client;
    } catch (e) {
      console.log(e)
    }
  }

  openAccountModal() {
    this.bsModalRef = this.modalService.show(AccountModalComponent, {
      keyboard: false,
      ignoreBackdropClick: true,
      initialState: {
        client: this.client
      }
    });
    this.subsciption = this.modalService.onHide.subscribe((reason: string) => {
      this.getClient(this.client.id);
    })
  }

  ngOnDestroy() {
    if (this.subsciption) this.subsciption.unsubscbe();
  }
}
