import { AccountService } from '../account.service';
import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-account-modal',
  templateUrl: './account-modal.component.html',
  styleUrls: ['./account-modal.component.scss']
})
export class AccountModalComponent implements OnInit {
  client: any;
  public newClient: any;

  constructor(
    public bsModalRef: BsModalRef,
    private accService: AccountService
  ) { }

  ngOnInit() {
    this.newClient = Object.assign({}, this.client);
  }

  async changeAcc() {
    try {
      await this.accService.postReq('editClient', this.newClient)
      this.bsModalRef.hide();
    } catch (err) {
      console.log(err)
    }
  }
  getAccCurrency() {
    if (this.newClient.available_acc) {
      var currency: string;
      this.newClient.available_acc.map((account) => {
        if (account.acc === this.newClient.current_acc) currency = account.currency
      })
      return currency;
    } else {
      return null;
    }
  }

}
