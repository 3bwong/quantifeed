import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

// Custom Components
import { ContainerComponent } from './components/container/container.component';
import { AccountModalComponent } from './account-modal/account-modal.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ButtonComponent } from './components/button/button.component';
import { PaperComponent } from './components/paper/paper.component';
import { AppComponent } from './app.component';

import { AccountService } from './account.service';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ButtonComponent,
    PaperComponent,
    ContainerComponent,
    AccountModalComponent
  ],
  imports: [
    FontAwesomeModule,
    HttpClientModule,
    BrowserModule,
    FormsModule,
    ModalModule.forRoot(),
  ],
  providers: [
    AccountService
  ],
  entryComponents:[
    AccountModalComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
