import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
  @Input() type: string;
  @Input() color: string;
  @Input() background: string;

  constructor() { }

  ngOnInit() {
    if(!this.color) this.color = (this.type === 'filled') ? '#f9f9f9' : '#333';
    if(!this.background) this.background = (this.type === 'filled') ? '#1d2536' : 'transparent';
  }

  buttonClass() {
    var btnClass: string;
    switch (this.type) {
      case 'icon':
        btnClass = 'button-icon';
        break;
      case 'outline':
        btnClass = 'button-outline';
        break;
      case 'filled':
        btnClass = 'button-filled';
        break;
      default:
        btnClass = 'button-transparent';
        break;
    }
    return btnClass;
  }

}
