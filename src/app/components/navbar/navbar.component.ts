import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  @Input() color: string = '#f9f9f9';
  @Input() background: string = '#1d2536';

  constructor() { }

  ngOnInit() {
  }

}
