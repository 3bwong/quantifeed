import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const url = "/api/";

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  public headers: HttpHeaders

  constructor(
    private http: HttpClient
  ) {
    this.headers = new HttpHeaders({'Content-type': 'application/json'});
  }

  getReq(uri){
    return new Promise ((resolve,reject)=>{
      this.http.get(url+uri, {headers: this.headers}).toPromise().then((res: any)=>{
        resolve(res);
      }).catch((err)=>{
        reject(err);
      })
    })
  }
  postReq(uri, body){
    return new Promise ((resolve,reject)=>{
      this.http.post(url+uri, body, {headers: this.headers}).toPromise().then((res: any)=>{
        resolve(res);
      }).catch((err)=>{
        reject(err);
      })
    })
  }

}
