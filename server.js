const express = require('express');
const path = require('path');
const app = express();

const client = require('./api/client');
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});
app.use(express.json())

app.get('/api/getClient', client.getClient);
app.post('/api/editClient', client.editClient);

app.use(express.static(__dirname + '/dist/quantifeed'));
app.get('/*', function (req, res) {
    res.sendFile(path.join(__dirname + '/dist/quantifeed/index.html'));
});

app.listen(process.env.PORT || 8080);