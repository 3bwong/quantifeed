# Quantifeed Assessment
This project is a single page built using components from a SPA - [Angular](https://angular.io/). This project aims to implement a popup to change clients' account numbers. The popup is implement with the help of [ngx-bootstrap](https://valor-software.com/ngx-bootstrap/#/). See [Demo](https://quantifeed-assessment.herokuapp.com/).

## Getting Started
This is an instruction for run and build the project on local machine.

### Prerequisities
This project is based in [Angular](https://angular.io/) and generated with [Angular CLI](https://cli.angular.io/). Please install both before start. More usages of Angular CLI can be found in [angular.md](angular.md).

### Installation
Download the current project. Then install all the required npm packages.
```
npm install
```
Run the following script to start the server.
```
npm start
```
Then, open the app on localhost:8080.

## Components
### App Container [app-container]
The App Container will take only app-navbar and container-content into the components. Other than those, nothing would be shown.

* Input
    * fixHeader
		* Type: boolean
		* Default: true
		* Description: Determine if the page header is fixed on top all time.
	* Method: Attach the input attribute to component (app-container) and set value.
* Web Component
    * app-navbar
		* Type: Custom Component
		* Description: The container will take in the navbar and stay it on top.
	* Method: Add this component inside app-container.
* Attribute
    * container-content
		* Description: The container will take in this as the content with pre set style.
	* Method: Attach to any div
	```
	<div container-content></div>
	```

Sample Usage:
```
<app-container [fixHeader]="true">
    <app-navbar>
        <span>Title</span>
    </app-navbar>
    <div container-content>
        content
    </div>
</app-container>
```

### App Navbar [app-navbar]
* Input
    * color
		* Type: string
		* Default: '#f9f9f9'
		* Description: it takes in string to set font color. eg. '#fff', 'rbga(0,0,0,0)'.
    * background
		* Type: string
		* Default: '#1d2536'
		* Description: it takes in string to set background color. eg. '#fff', 'rbga(0,0,0,0)'.
	* Method: Attach the input attribute to component (app-navbar) and set value.

* Class
    * navbar-heading
		* Description: Default style for Title of the Web App.
    * navbar-spacer - Style to separate blocks.
		* Description: Style to separate blocks.
	* Method: Attach a class with above value.

Sample Usage:
```
<app-navbar color="#eee" background="#333">
    <span class="navbar-heading"> Title </span>
    <span class="navbar-spacer"></span>
    <span> Other Nav </span>
</app-navbar>
```

### App Button [app-button]
* Attribute
    * paper-heading
		* Description: This is setting a paper heading to default style.
	* Method: Attach to any div
	```
	<div paper-heading></div>
	```
* Class
    * papaer-spacer
		* Description: This is used to separating blocks inside paper-heading. This works only inside paper-heading.
	* Method: Attach a class to span with above value.

Sample Usage:
```
<app-button type="outline" color="#555"> Outline Button </app-button>
<app-button type="filled" background="#309952"> Filled Button </app-button>
<app-button color="#f9f9f9" type="transparent"> Transparent Button </app-button>
<app-button type="icon" color="#659fcd"><i> ICON </i></app-button>
```

### App Paper [app-paper]
* Input
    * type
        * Type: string
        * Default: 'transparent'
        * Description: Set the style of the button with transparent, outline, filled and icon.
    * color
		* Type: string
		* Default: '#f9f9f9' (if type !== 'filled') / '#333' (if type === 'filled)
		* Description: it takes in string to set font color. eg. '#fff', 'rbga(0,0,0,0)'.
    * background
		* Type: string
		* Default: '#1d2536' (if type === 'filled') / 'transparent' (if type !== 'filled)
		* Description: it takes in string to set background color. eg. '#fff', 'rbga(0,0,0,0)'.
	* Method: Attach the input attribute to component (app-button) and set value.

Sample Usage:
```
<app-paper>
    <div paper-heading>
    <h3> Title </h3>
    <span class="paper-spacer"></span>
    <div> Other Blocks / Content </div>
    </div>
    <div> Content... </div>
</app-paper>
```

## Testing
The testing is provided by Angular CLI package. Run the following script.
```
ng test
```

## Built With
* [Angular](https://angular.io/) - Single Page Application
* [Ngx-Bootstrap](https://valor-software.com/ngx-bootstrap/#/) - Pre-built Web Component for Angular (Mainly used for popup)
* [Express](https://github.com/expressjs/express) - Proxy Server for API and Web App

## License
This project is licensed under the ISC License - see the [LICENSE](LICENSE) file for details