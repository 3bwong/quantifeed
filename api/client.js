const fs = require('fs');
exports.getClient = (req, res) => {
    var data = require('./data.json');
    var reqClient = data.clients.find((client) => {
        return client.id === req.query.client;
    })
    res.status(200).send({ client: reqClient })
}

exports.editClient = (req, res) => {
    var data = require('./data.json');
    data.clients = data.clients.map((client) => {
        if (client.id === req.body.id) return req.body;
    })
    fs.writeFileSync(__dirname + '/data.json', JSON.stringify(data));
    res.status(200).send({message: "Client Updated"});
}